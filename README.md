# Tiled Map Loader

This project aims to be a library to support better and easier Tiled maps. 
This library:

* should load and create a `FlxSpriteGroup` for each layer.
* may add layers directly to a `FlxState` (optional).
* may create polygonal colliders using `Nape` library (since this can be processor-intensive, should be optional).
* should load object layers (a callback will be used to load each object).
* supports image layers.
* supports animated tiles
* supports flipped tiles

This library will (maybe, eventually, some day, with some effort of some people):

* support hexagonal tiles
* support isometric tiles
* support staggered tiles


Collision polygon will be supported only when using `Nape`.
